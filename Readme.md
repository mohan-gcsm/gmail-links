This is project is a python based application to retrieve any and all mail lilnks that are ever sent or received on ur gmail inbox.

The following are the dependency libraries. Use PIP or EASY_INSTALL or BREW to install the below.

	re
	json
	pymongo
	base64
	dateutil

Make sure "Mongo" running with following values:

	MONGO_HOST = '127.0.0.1'
	MONGO_PORT = 27017
	MONGO_USERNAME = ''
	MONGO_PASSWORD = ''

Running Program: 

	# python start.py


This application requires client_secret.json which can be obtained from https://developers.google.com/gmail/api/quickstart/python