from __future__ import print_function
import httplib2
import os, json, re

from base64 import b64encode, b64decode
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
import dateutil.parser as parser
from bs4 import BeautifulSoup
from pymongo import MongoClient

#-----mongodb config--------#
MONGO_HOST = '127.0.0.1'
MONGO_PORT = 27017
MONGO_USERNAME = ''
MONGO_PASSWORD = ''

db_client = MongoClient(MONGO_HOST,MONGO_PORT)
db=db_client.mail_links         # db initialization


try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at .credentials/creds.json
SCOPES = 'https://www.googleapis.com/auth/gmail.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Gmail API Python App'

IGNORE_DOMAINS = [
                    'sendgrid.com',
                    'jira.fkinternal.com',
                    'w3.org',
                    'www.flipkart.com'
                ]

labels_list = []

def jsonPrint(json_dict):
    print(json.dumps(json_dict, sort_keys=True, indent=4, separators=(',', ': ')))

def extract_link(text):
    regex = r'href="https?://[^\s<>"]+|www\.[^\s<>"]+'
    match = re.findall(regex, text)
    if match:
        for ele in match:
            for domain in IGNORE_DOMAINS:
                if domain in ele:
                    match.pop(ele)
        return match
    return "None"


def get_credentials():
    home_dir = os.getcwd()

    credential_dir = os.path.join(home_dir, '.credentials')
    
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)

    credential_path = os.path.join(credential_dir,
                                   'creds.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def main():

    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)
    labels = None

    ##### Code for fetching label details

    # results = service.users().labels().list(userId='me').execute()
    # labels = results.get('labels', [])

    # if not labels:
    #     print('No labels found.')
    # else:
    #   print('Labels:')
    #   for label in labels:
    #     labels_list.append(label['name'])

    # labels = labels_list

    # print (labels)

    results = service.users().messages().list(userId='me', labelIds=labels, q=None, pageToken=None, maxResults=None, includeSpamTrash=None).execute()
    messages = results.get('messages', [])
    
    if not messages:
        print('No messages found.')
    else:
        for mssg in messages:
            temp_dict = {}
            m_id = mssg['id'] # get id of individual message
            message = service.users().messages().get(userId='me', id=m_id).execute() # fetch the message using API
            payld = message['payload'] # get payload of the message 
            headr = payld['headers'] # get header of the payload


            for one in headr: # getting the Subject
                if one['name'] == 'Subject':
                    msg_subject = one['value']
                    temp_dict['Subject'] = msg_subject
                else:
                    pass


            for two in headr: # getting the date
                if two['name'] == 'Date':
                    msg_date = two['value']
                    date_parse = (parser.parse(msg_date))
                    m_date = (date_parse.date())
                    temp_dict['Date'] = str(m_date)
                else:
                    pass

            for three in headr: # getting the Sender
                if three['name'] == 'From':
                    msg_from = three['value']
                    temp_dict['Sender'] = msg_from
                else:
                    pass

            temp_dict['Snippet'] = message['snippet'] # fetching message snippet


            try:
                
                # Fetching message body
                mssg_parts = payld['parts'] # fetching the message parts
                part_one  = mssg_parts[0] # fetching first element of the part 
                part_body = part_one['body'] # fetching body of the message
                part_data = part_body['data'] # fetching data from the body
                clean_one = part_data.replace("-","+") # decoding from Base64 to UTF-8
                clean_one = clean_one.replace("_","/") # decoding from Base64 to UTF-8
                clean_two = b64decode(clean_one) # decoding from Base64 to UTF-8
                
                temp_dict['links'] = extract_link(str(clean_two))
                                
            except Exception, ae :
                temp_dict['links'] = "None"
                pass

            if temp_dict['links'] is not "None" and db.links.count({'Subject': temp_dict['Subject'], 'links': temp_dict['links']}) != 0:
                db.links.insert(temp_dict)

            jsonPrint(temp_dict)
            

if __name__ == '__main__':
    try:
        main()
    except:
        pass










